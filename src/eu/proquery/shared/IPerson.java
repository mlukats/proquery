package eu.proquery.shared;

public interface IPerson {
	
	String getName();
	void setName(String name);
	
	String getPhoneNumber();
	void setPhoneNumber(String phonenumber);

}
