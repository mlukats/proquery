package eu.proquery.shared;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import eu.proquery.client.dto.Person;


public interface IResponseDTO extends Serializable{

	long getEndDate();
	
	boolean getActive();
	
	String getLanguage();
	
	String getPhoneNumber();
	
	String getSequenceNumber();
	/*
	 *if no list was present returns null
	 */
	List<Person> getOverRideList();

	boolean getXlAdditionalService();
	
	String getXlAdditionalServiceLanguage();

	long getXlAdditionalServiceActivationTime();
	
	long getXlAdditionalServiceEndTime();
	

	



}
