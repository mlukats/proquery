package eu.proquery.client;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

@RemoteServiceRelativePath("serverservices")
public interface ServerServices extends RemoteService {
	
	String getJSON();

}
