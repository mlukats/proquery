package eu.proquery.client.dto;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.json.client.JSONArray;
import com.google.gwt.json.client.JSONObject;
import com.google.gwt.json.client.JSONValue;

import eu.proquery.shared.IResponseDTO;


public class ResponseDTO implements IResponseDTO {


	private static final long serialVersionUID = 1L;
	private boolean active;
	private long endDate;
	private String language;
	private String phoneNumber;
	private String sequenceNumber;
	private List<Person> overRideList;
	private boolean xlAdditionalService;
	private String xlAdditionalServiceLanguage;
	private long xlAdditionalServiceActivationTime;
	private long xlAdditionalServiceEndTime;

	/**
	 * Convert JSON object to ResponseDTO
	 * @param jsonObject
	 * @return
	 */
	public static ResponseDTO create(JSONObject jsonObject){

		ResponseDTO responseDto = new ResponseDTO();


		JSONValue activeVal = jsonObject.get("active");
		JSONValue endDateVal = jsonObject.get("serviceenddatetime");
		JSONValue overRideListVal = jsonObject.get("overridelist");
		JSONValue languageVal = jsonObject.get("language");
		JSONValue phoneNumberVal = jsonObject.get("phonenumber");
		JSONValue sequenceNumberVal = jsonObject.get("sequencenumber");
		JSONValue xlAdditionalServiceVal = jsonObject.get("xladditionalservice");
		JSONValue xlAdditionalServiceLanguageVal = jsonObject.get("xladditionalservicelanguage");
		JSONValue xlAdditionalServiceActivationTimeVal = jsonObject.get("xladditionalserviceactivationtime");
		JSONValue xlAdditionalServiceEndTimeVal = jsonObject.get("xladditionalserviceendtime");

		if(activeVal != null && activeVal.isBoolean() != null){
			boolean active = activeVal.isBoolean().booleanValue();
			responseDto.setActive(active);
		}

		if(endDateVal != null && endDateVal.isNumber() != null){
			long endDate = (long) endDateVal.isNumber().doubleValue();
			responseDto.setEndDate(endDate);
		}

		if(languageVal != null && languageVal.isString() != null){
			String language = languageVal.isString().stringValue();
			responseDto.setLanguage(language);
		}
		if(phoneNumberVal != null && phoneNumberVal.isString() != null){
			String phoneNumber = phoneNumberVal.isString().stringValue();
			responseDto.setPhoneNumber(phoneNumber);	
		}

		if(overRideListVal != null && overRideListVal.isArray() != null){
			List<Person> overRideList = overRideList(overRideListVal);
			responseDto.setOverRideList(overRideList);
		}

		if(sequenceNumberVal != null && sequenceNumberVal.isString() != null){
			String sequenceNumber = sequenceNumberVal.isString().stringValue();
			responseDto.setSequenceNumber(sequenceNumber);
		}

		if(xlAdditionalServiceVal != null && xlAdditionalServiceVal.isString() != null){
			boolean xlAdditionalService = xlAdditionalServiceVal.isBoolean().booleanValue();
			responseDto.setXlAdditionalService(xlAdditionalService);
		}
		
		if(xlAdditionalServiceLanguageVal != null && xlAdditionalServiceLanguageVal.isString() != null){
			String xlAdditionalServiceLanguage = xlAdditionalServiceLanguageVal.isString().stringValue();
			responseDto.setXlAdditionalServiceLanguage(xlAdditionalServiceLanguage);
		}
		
		if(xlAdditionalServiceActivationTimeVal != null && xlAdditionalServiceActivationTimeVal.isNumber() != null){
			double xlAdditionalServiceActivationTime = xlAdditionalServiceActivationTimeVal.isNumber().doubleValue();
			responseDto.setXlAdditionalServiceActivationTime(xlAdditionalServiceActivationTime);
		}
		
		if(xlAdditionalServiceEndTimeVal != null && xlAdditionalServiceEndTimeVal.isNumber() != null){
			double xlAdditionalServiceEndTime = xlAdditionalServiceEndTimeVal.isNumber().doubleValue();
			responseDto.setXlAdditionalServiceEndTime(xlAdditionalServiceEndTime);
		}
		
		return responseDto;
	}

	private static List<Person> overRideList(JSONValue value){

		JSONArray jsonArray = value.isArray();
		List<Person> list = new ArrayList<Person>();

		if (jsonArray != null) {
			for(int i = 0; i < jsonArray.size(); i++){
				if(jsonArray.get(i) instanceof JSONObject){

					String name = jsonArray.get(i).isObject().get("name").isString().stringValue();
					String phoneNumber = jsonArray.get(i).isObject().get("phonenumber").isString().stringValue();

					Person person = new Person( name, phoneNumber);

					list.add(person);
				}
			}
		}

		return list;
	}



	@Override
	public String getSequenceNumber() {
		return sequenceNumber;
	}

	@Override
	public String getPhoneNumber() {
		return phoneNumber;
	}

	@Override
	public boolean getActive() {
		return active;
	}

	@Override
	public long getEndDate() {
		return endDate;
	}

	@Override
	public List<Person> getOverRideList() {
		return overRideList;
	}

	public void setSequenceNumber(String sequenceNumber) {
		this.sequenceNumber = sequenceNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	/*	public void setStarDate(long starDate) {
		this.starDate = starDate;
	}*/

	public void setEndDate(long endDate) {
		this.endDate = endDate;
	}

	public void setOverRideList(List<Person> overRideList) {
		this.overRideList = overRideList;
	}

	public void setActive(boolean active){
		this.active = active;
	}

	@Override
	public String getLanguage() {
		// TODO Auto-generated method stub
		return language;
	}

	/**
	 * @param language the language to set
	 */
	public void setLanguage(String language) {
		this.language = language;
	}

	@Override
	public boolean getXlAdditionalService() {
		return xlAdditionalService;
	}

	@Override
	public String getXlAdditionalServiceLanguage() {
		return xlAdditionalServiceLanguage;
	}

	@Override
	public long getXlAdditionalServiceActivationTime() {
		return xlAdditionalServiceActivationTime;
	}

	@Override
	public long getXlAdditionalServiceEndTime() {
		return xlAdditionalServiceEndTime;
	}
	public void setXlAdditionalService(boolean xlAdditionalService) {
		this.xlAdditionalService = xlAdditionalService;
	}

	public void setXlAdditionalServiceLanguage(String xlAdditionalServiceLanguage) {
		this.xlAdditionalServiceLanguage = xlAdditionalServiceLanguage;
	}

	public void setXlAdditionalServiceActivationTime(
			double xlAdditionalServiceActivationTime) {
		this.xlAdditionalServiceActivationTime = (long) xlAdditionalServiceActivationTime;
	}

	public void setXlAdditionalServiceEndTime(double xlAdditionalServiceEndTime) {
		this.xlAdditionalServiceEndTime = (long) xlAdditionalServiceEndTime;
	}


}
