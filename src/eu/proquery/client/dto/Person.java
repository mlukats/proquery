package eu.proquery.client.dto;

import eu.proquery.shared.IPerson;

public class Person implements IPerson {

	private String name;
	private String phonenumber;
	
	public Person(String name, String phonenumber) {
		setName(name);
		setPhoneNumber(phonenumber);
	}
	

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPhoneNumber() {
		return phonenumber;
	}

	public void setPhoneNumber(String phonenumber) {
		this.phonenumber = phonenumber;
	}

}
