package eu.proquery.client.presenters;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.json.client.JSONParser;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Panel;

import eu.proquery.client.ServerServices;
import eu.proquery.client.ServerServicesAsync;
import eu.proquery.client.dto.ResponseDTO;
import eu.proquery.client.presenters.IDataView.IDataPresenter;



public class DataPresenter implements IDataPresenter {

	private ServerServicesAsync serverServicesAsync = GWT.create(ServerServices.class);

	private IDataView view;
	private Panel mainPanel;

	public DataPresenter(IDataView view, Panel panel, ServerServicesAsync serverServices ) {

		this.view = view;

		this.mainPanel = panel;

		bind();
	}

	private void bind() {

		
		doDataShow();

		mainPanel.add(view);
	}

	private void doDataShow() {
		serverServicesAsync.getJSON(new AsyncCallback<String>() {

			@Override
			public void onSuccess(String result) {

				if(result == null || result.equals("")){
					System.out.println("Result val: " + result);
					view.DisplaySearviceNotReachable();
				}
				else{
					view.Display(ResponseDTO.create(JSONParser.parseLenient(result).isObject()));
				}
			}

			@Override
			public void onFailure(Throwable caught) {
				view.DisplaySearviceNotReachable();

			}
		});

	}












}














