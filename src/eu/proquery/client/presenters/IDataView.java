package eu.proquery.client.presenters;

import java.util.List;

import com.google.gwt.user.client.ui.IsWidget;

import eu.proquery.client.dto.Person;
import eu.proquery.shared.IResponseDTO;

public interface IDataView extends IsWidget {


	interface IDataPresenter{


	}

	void Display(IResponseDTO result);

	void setSequenceNumber(int id);

	void setLanguage(String language);

	void setPhone(String phoneNumber);

	void setServiceStatus(boolean active, long endDate);

	void setXLService(boolean active, long activationTime,
			long endTime, String xlServiceLanguage);

	void setInActiveXLService();

	void DisplayOverRideList(List<Person> list);

	void DisplaySearviceNotReachable();

}
