package eu.proquery.client;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.ui.RootPanel;

import eu.proquery.client.presenters.DataPresenter;
import eu.proquery.client.presenters.IDataView;
import eu.proquery.client.ui.DataView;

/**
 * Entry point classes define <code>onModuleLoad()</code>.
 */
public class Proquery implements EntryPoint {
	
	private final ServerServicesAsync serverServices = GWT
			.create(ServerServices.class);

	public void onModuleLoad() {
		
		IDataView view = new DataView();
		
		DataPresenter presenter = new DataPresenter(view, RootPanel.get(), serverServices);
		
	}
}
