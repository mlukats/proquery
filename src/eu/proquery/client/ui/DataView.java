package eu.proquery.client.ui;

import java.util.Date;
import java.util.List;
import java.util.Map;

import com.google.gwt.core.client.GWT;
import com.google.gwt.i18n.shared.DateTimeFormat;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.cellview.client.CellTable;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.Widget;

import eu.proquery.client.dto.Person;
import eu.proquery.client.presenters.IDataView;
import eu.proquery.shared.IResponseDTO;

public class DataView extends Composite implements IDataView {

	private static DataViewUiBinder uiBinder = GWT
			.create(DataViewUiBinder.class);

	interface DataViewUiBinder extends UiBinder<Widget, DataView> {
	}

	@UiField FlowPanel parameters;
	@UiField FlowPanel overRideListContainer;
	@UiField CellTable<Person> overRideList;
	@UiField HTMLPanel htmlPanel;

	public DataView() {
		
		
		
		initWidget(uiBinder.createAndBindUi(this));
		overRideListContainer.setVisible(false);
		createOverRideList();
	}

	private CellTable<Person> createOverRideList(){
		
		TextColumn<Person> phoneColumn = new TextColumn<Person>() {

			@Override
			public String getValue(Person person) {
				return person.getPhoneNumber();
			}

		};

		TextColumn<Person> nameColumn = new TextColumn<Person>() {

			@Override
			public String getValue(Person person) {
				return person.getName();
			}

		};

		overRideList.addColumn(phoneColumn, "Phone");

		overRideList.addColumn(nameColumn, "Name");
		
		return overRideList;
	}

	public void DisplayParam(Map<String, String> map) {

		if (map == null) {
			
			parameters.clear();
			parameters.setVisible(false);
			
			return;
		}

		parameters.setVisible(true);

		for (Map.Entry<String, String> entry : map.entrySet()) {

			HTML param = new HTML(entry.getKey() + ": " + entry.getValue());

			parameters.add(param);
		}

	}

	@Override
	public void DisplayOverRideList(List<Person> list) {

		
		if (list == null || list.size() == 0) {

			overRideListContainer.setVisible(false);
			try {
			overRideList.setRowData(0, list);
			} catch (NullPointerException e) {}
			return;
		}

		overRideListContainer.setVisible(true);
		
		overRideList.setRowData(0, list);
		
	}

	@Override
	public void Display(IResponseDTO result) {

		displayParam(result);

		DisplayOverRideList(result.getOverRideList());

	}

	@Override
	public void DisplaySearviceNotReachable() {

		this.parameters.clear();
		
		setNoServiceAvaliable();
		
		DisplayOverRideList(null);
		
		
		

	}
	
	public void displayParam(IResponseDTO result) {

		this.parameters.clear();

		if (result.getSequenceNumber() != null
				&& !result.getSequenceNumber().trim().equals("")) {
			setSequenceNumber(Integer.parseInt(result.getSequenceNumber()));
		}

		if (result.getPhoneNumber() != null
				&& !result.getPhoneNumber().trim().equals("")) {
			setPhone(result.getPhoneNumber());
		}

		if (result.getLanguage() != null && result.getActive())
			setLanguage(result.getLanguage());

		if (result.getActive()) {
			setServiceStatus(result.getActive(), result.getEndDate());

		} else {
			setServiceStatus(result.getActive(), result.getEndDate());
			return;
		}

		if (result.getXlAdditionalService()) {
			setXLService(result.getXlAdditionalService(),
					result.getXlAdditionalServiceActivationTime(),
					result.getXlAdditionalServiceEndTime(),
					result.getXlAdditionalServiceLanguage());
		} else {
			setInActiveXLService();
		}

	}
	
	private void setNoServiceAvaliable() {
		HTML param = new HTML("Service not reached or not available");
		parameters.add(param);
		
	}

	@Override
	public void setXLService(boolean active, long activationTime, long endTime,
			String xlServiceLanguage) {

		StringBuilder sBuilder = new StringBuilder("XL-service active");

		if (activationTime != 0) {

			DateTimeFormat dtf = DateTimeFormat.getFormat("HH:mm");
			Date dateActiveStart = new Date();
			Date dateActiveEnd = new Date();

			dateActiveStart.setTime(activationTime);
			dateActiveEnd.setTime(endTime);

			sBuilder.append(" (" + dtf.format(dateActiveStart) + "-"
					+ dtf.format(dateActiveEnd) + ")");
		}

		if (xlServiceLanguage != null && !xlServiceLanguage.trim().equals("")) {

			sBuilder.append(" in " + xlServiceLanguage + " language");
		}

		HTML param = new HTML(sBuilder.toString());
		parameters.add(param);

	}

	@Override
	public void setLanguage(String language) {
		HTML param = new HTML("Language: " + language);
		parameters.add(param);
	}

	@Override
	public void setPhone(String phoneNumber) {
		HTML param = new HTML("Phone: " + phoneNumber);
		parameters.add(param);
	}

	@Override
	public void setSequenceNumber(int id) {
		HTML param = new HTML("Request sequence id: " + id);
		parameters.add(param);

	}

	@Override
	public void setServiceStatus(boolean active, long endDate) {

		StringBuilder serviceString = new StringBuilder();
		HTML param = null;

		if (active) {

			serviceString.append("Service active");

			serviceString.append(" ");

			Date date = new Date();
			date.setTime(endDate);
			
			DateTimeFormat format = DateTimeFormat.getFormat("MMMM dd yyyy HH:mm");

			serviceString.append(format.format(date));

			param = new HTML(serviceString.toString());

		} else {

			serviceString.append("Service inactive");
			param = new HTML("Service " + serviceString.toString());
		}
		parameters.add(param);

	}

	@Override
	public void setInActiveXLService() {
		HTML param = new HTML("XL-service inactive");
		parameters.add(param);

	}

}
