package eu.proquery.client;

import com.google.gwt.user.client.rpc.AsyncCallback;

public interface ServerServicesAsync {
	
	void getJSON(AsyncCallback<String> callback);

}
