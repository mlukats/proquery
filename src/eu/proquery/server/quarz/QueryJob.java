package eu.proquery.server.quarz;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import eu.proquery.server.Globals;
import eu.proquery.server.RequestData;

public class QueryJob implements Job {

	private static int index = 1;
	
	@Override
	public void execute(JobExecutionContext arg0) throws JobExecutionException {
		
		if(index > 9) index = 1;
		
		Globals.legacyServiceResponse = new String[] {RequestData.request(index), Integer.toString(index)};
		
		index++;
	}

}
