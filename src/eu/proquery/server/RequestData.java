package eu.proquery.server;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;


import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.jsoup.HttpStatusException;

public class RequestData {

	public static String request(int id){
		
	
		
		String responseString = "";
		

		try {

			String urlString ="http://people.proekspert.ee/ak/data_" +
								id + ".txt";
			
			URL url = new URL(urlString);
			
			Document doc = Jsoup.parse(url, 0);
			
			Elements elements = doc.getElementsByTag("body");

			if(elements.size() == 1){
				
				responseString = elements.get(0)
						.textNodes().get(0).getWholeText();
				
			}

		} catch (HttpStatusException e) {
			return "";
		} catch (MalformedURLException e) {
			return "";
		} catch (IOException e) {
			return "";
		}

		return responseString;
		
	}

}
