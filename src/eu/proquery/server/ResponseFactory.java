package eu.proquery.server;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

public class ResponseFactory {

	/*
	 * 
	 * Constructs a json string from string.
	 */
	public static String getJSONString(String recivedResponse, final String id) {
		JSONObject responJSObject = new JSONObject();

		try {

			responJSObject.put("sequencenumber", getSequenceNumber(id));
			
			if (recivedResponse == null || recivedResponse.equals(""))
				return responJSObject.toString();

			char[] response = recivedResponse.toCharArray();
			
			responJSObject.put("active", getActive(response[0]));
			if (response.length >= 21) {
				responJSObject.put("phonenumber",
						getPhoneNumber(getArray(response, 1, 20)));
			}
			if (response.length >= 22) {
				responJSObject.put("xladditionalservice",
						getXlAdditionalService(response[21]));
			}

			if (response.length >= 23) {
				responJSObject.put("language", getLanguage(response[22]));
			}

			if (response.length >= 24) {
				responJSObject.put("xladditionalservicelanguage",
						getXlAdditionalServiceLanguage(response[23]));
			}

			if (response.length >= 36) {
				responJSObject.put("serviceenddatetime",
						serviceEndDateTime(getArray(response, 24, 36)));
			}

			if (response.length >= 40) {
				responJSObject.put(
						"xladditionalserviceactivationtime",
						getXlAdditionalServiceActivationTime(getArray(response,
								36, 40)));
			}

			if (response.length >= 45) {
				responJSObject
				.put("xladditionalserviceendtime",
						getXlAdditionalServiceEndTime(getArray(
								response, 40, 44)));
			}

			// Added the test for 'K', not sure if nessesary, should read the
			// docs more and know more about the project.
			if(response.length >= 45 && response[44] == 'K'){
				if (response.length >= 325){
					responJSObject.put("overridelist",
							getOverRideList(getArray(response, 45, 325)));
					}
			}


		} catch (JSONException e) {
			e.printStackTrace();
		}

		return responJSObject.toString();
	}

	private static boolean getActive(char active) {

		if (active == 'A')
			return true;

		return false;
	}

	private static String getLanguage(char response) {

		if (response == 'E') {
			return "Estonian";
		}
		if (response == 'I') {
			return "English";
		}

		return "";
	}

	private static String getPhoneNumber(char[] response) {

		return getString(response);

	}

	private static Long serviceEndDateTime(char[] response) {

		if (isInteger(new String(response))) {

			int year = Integer.parseInt(getString(getArray(response, 0, 4)));
			int month = Integer.parseInt(getString(getArray(response, 4, 5)));
			int day = Integer.parseInt(getString(getArray(response, 5, 6)));
			int hrs = Integer.parseInt(getString(getArray(response, 6, 8)));
			int min = Integer.parseInt(getString(getArray(response, 8, 10)));

			Calendar dateTime = new GregorianCalendar();
			dateTime.set(year, month, day, hrs, min);

			return dateTime.getTimeInMillis();

		}

		return (long) 0;

	}

	private static String getSequenceNumber(String id) {

		return id;
	}

	private static List<Person> getOverRideList(char[] response) {

		int phoneNumberStep = 15;

		int nameStep = 20;

		ArrayList<Person> list = new ArrayList<Person>();

		for (int i = 0, j = 120; i < 120; i += phoneNumberStep, j += nameStep) {

			String phoneNumber = getString(response, i, i + phoneNumberStep);

			if (phoneNumber.equals(""))
				continue;

			String name = getString(response, j, j + phoneNumberStep);

			Person person = new Person(name, phoneNumber);

			list.add(person);

		}
		return list;

	}

	private static boolean getXlAdditionalService(char response) {

		if (response == 'J') {
			return true;
		}

		return false;
	}

	/*
	 * Return time in milliseconds
	 */
	private static long getXlAdditionalServiceActivationTime(char[] response) {

		char[] time = getArray(response, 0, 4);

		if (isInteger(new String(time))) {

			Calendar dateTime = new GregorianCalendar();

			int hrs = Integer.parseInt(new String(getArray(time, 0, 2)));
			int min = Integer.parseInt(new String(getArray(time, 2, 4)));

			dateTime.set(0, 0, 0, hrs, min);

			return dateTime.getTimeInMillis();
		}

		return 0;
	}

	/*
	 * Return time in milliseconds
	 */
	private static long getXlAdditionalServiceEndTime(char[] response) {

		char[] time = getArray(response, 0, 4);

		if (isInteger(new String(time))) {

			Calendar dateTime = new GregorianCalendar();

			int hrs = Integer.parseInt(new String(getArray(time, 0, 2)));
			int min = Integer.parseInt(new String(getArray(time, 2, 4)));

			dateTime.set(0, 0, 0, hrs, min);

			return dateTime.getTimeInMillis();
		}

		return 0;
	}

	private static String getXlAdditionalServiceLanguage(char response) {

		if (response == 'E') {
			return "Estonian";
		}
		if (response == 'I') {
			return "English";
		}

		return "" + response;
	}

	private static String getString(char[] chars) {

		return getString(chars, 0, chars.length);
	}

	private static String getString(char[] chars, int start, int end) {

		StringBuilder sBuilder = new StringBuilder();

		for (int i = start; i < end; i++) {

			sBuilder.append(chars[i]);

		}

		return sBuilder.toString().trim();
	}

	private static char[] getArray(char[] response, int start, int end) {

		if (end <= 0 || end < start)
			return null;

		char[] array = Arrays.copyOfRange(response, start, end);

		return array;
	}

	public static boolean isInteger(String str) {

		if (str == null) {
			return false;
		}
		int length = str.length();
		if (length == 0) {
			return false;
		}
		int i = 0;
		if (str.charAt(0) == '-') {
			if (length == 1) {
				return false;
			}
			i = 1;
		}
		for (; i < length; i++) {
			char c = str.charAt(i);
			if (c <= '/' || c >= ':') {
				return false;
			}
		}
		return true;
	}

}
