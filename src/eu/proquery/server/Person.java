package eu.proquery.server;

import org.json.JSONException;
import org.json.JSONObject;

import eu.proquery.shared.IPerson;

public class Person extends JSONObject implements IPerson {
	
	public Person(String name, String phonenumber) {
		setName(name);
		setPhoneNumber(phonenumber);
	}
	
	
	public String getName() {
		try {
			
			return this.get("name").toString();
			
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return "";
	}
	
	public void setName(String name) {
		try {
			this.put("name", name);
			
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}
	public String getPhoneNumber() {
		try {
			return this.get("phonenumber").toString();
			
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return "";
	}
	public void setPhoneNumber(String phonenumber) {
		try {
			this.put("phonenumber", phonenumber);
			
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}



	
	

}
